import * as PIXI from 'pixi.js';


const app_container = document.getElementById("app");
const app = new PIXI.Application(450, 300, {backgroundColor: 0x000000});
app_container.appendChild(app.view);

const assetsPath = 'https://static.independent.co.uk/s3fs-public/thumbnails/image/2017/09/12/11/naturo-monkey-selfie.jpg?w968';

const fetchAssets = fetch(assetsPath, {
    method: 'GET',
    mode: 'same-origin'
});
fetchAssets.then((response) => {
    return response.json()
})
    .then((response) => {
        console.log(response)
    });

PIXI.loader
    .add('spritesheet', assetsPath)
    .load(drawGraphics);

function drawGraphics(loader, res) {

    const header = new PIXI.Container();
    header.name = "[Widget] Header";
    console.log(loader,res);
    const logoISB = new PIXI.Texture.fromImage("https://static.independent.co.uk/s3fs-public/thumbnails/image/2017/09/12/11/naturo-monkey-selfie.jpg?w968");
    const logoSprite = new PIXI.Sprite(logoISB);
    header.addChild(logoSprite);

    app.stage.addChild(header);
}
