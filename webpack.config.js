const path = require('path');

const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: './src/index.js',
  output: {
      filename: 'index.js',
      path: path.resolve(__dirname, 'dist')
  },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    },
    plugins: [
        new CopyPlugin(
            [
                { from: './src/styles.css', to: '' },
                { from: './src/index.html', to: '' },
                { from: './src/assets', to: 'assets' },
            ]
        )
    ]
};
